package net.thumbtack.school.elections.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Candidate implements Serializable {
    private Voter voter;
    private List<Sentence> program;
    private boolean agreement;

    public Candidate(Voter voter) {
        this.voter = voter;
        this.program = new ArrayList<>();
        this.agreement = false;
    }

    public Voter getVoter() {
        return voter;
    }

    public void setVoter(Voter voter) {
        this.voter = voter;
    }

    public List<Sentence> getProgram() {
        return program;
    }

    public void setProgram(List<Sentence> program) {
        this.program = program;
    }

    public void setSentence(Sentence sentence) {
        this.program.add(sentence);
    }

    public boolean isAgreement() {
        return agreement;
    }

    public void setAgreement(boolean agreement) {
        this.agreement = agreement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Candidate candidate = (Candidate) o;
        return agreement == candidate.agreement &&
                Objects.equals(voter, candidate.voter) &&
                Objects.equals(program, candidate.program);
    }

    @Override
    public int hashCode() {
        return Objects.hash(voter, program, agreement);
    }

}
