package net.thumbtack.school.elections.model;

import java.io.Serializable;
import java.util.Objects;

public class Mark implements Serializable {
    private Voter author;
    private int mark;

    public Mark(Voter author, int mark) {
        this.author = author;
        this.mark = mark;
    }

    public Voter getAuthor() {
        return author;
    }

    public void setAuthor(Voter author) {
        this.author = author;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mark mark1 = (Mark) o;
        return mark == mark1.mark &&
                Objects.equals(author, mark1.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, mark);
    }

}
