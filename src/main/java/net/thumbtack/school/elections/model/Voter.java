package net.thumbtack.school.elections.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class Voter implements Serializable {
    private String token;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String street;
    private String homeNumber;
    private String apartmentNumber;
    private String login;
    private String password;

    public Voter(String firstName, String lastName, String patronymic,
                 String street, String homeNumber, String apartmentNumber, String login, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.street = street;
        this.homeNumber = homeNumber;
        this.apartmentNumber = apartmentNumber;
        this.login = login;
        this.password = password;
    }

    public Voter(String token, String firstName, String lastName, String patronymic,
                 String street, String homeNumber, String apartmentNumber, String login, String password) {
        this.token = token;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.street = street;
        this.homeNumber = homeNumber;
        this.apartmentNumber = apartmentNumber;
        this.login = login;
        this.password = password;
    }

    public void login(){
            this.token = (UUID.randomUUID().toString());
    }

    public void logout(){
        this.token = null;
    }

    public String getToken() {
        return token;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getStreet() {
        return street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Voter voter = (Voter) o;
        return homeNumber == voter.homeNumber &&
                apartmentNumber == voter.apartmentNumber &&
                Objects.equals(firstName, voter.firstName) &&
                Objects.equals(lastName, voter.lastName) &&
                Objects.equals(patronymic, voter.patronymic) &&
                Objects.equals(street, voter.street) &&
                Objects.equals(login, voter.login) &&
                Objects.equals(password, voter.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, patronymic, street, homeNumber, apartmentNumber, login, password);
    }

}
