package net.thumbtack.school.elections.model;

import java.io.Serializable;
import java.util.*;

public class Sentence implements Serializable {
    private String sentence;
    private Voter author;
    private List<Mark> mark;

    public Sentence(Voter author, String sentence) {
        this.sentence = sentence;
        this.author = author;
        this.mark = new ArrayList<>();
        this.mark.add(new Mark(author, 5));
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public Voter getAuthor() {
        return author;
    }

    public void setAuthor(Voter author) {
        this.author = author;
    }

    public void setMark(List<Mark> mark) {
        this.mark = mark;
    }

    public double getMark() {
        int sum = 0;
        for(Mark m: mark){
            sum+=m.getMark();
        }
        return (double) sum/mark.size();
    }

    public List<Mark> getMarkList(){
        return mark;
    }

    public void setMark(Voter voter, Integer mark) {
        this.mark.add(new Mark(voter, mark));
    }

    public boolean removeMark(Voter voter){
        return this.mark.remove(voter);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sentence sentence1 = (Sentence) o;
        return Objects.equals(sentence, sentence1.sentence) &&
                Objects.equals(author, sentence1.author) &&
                Objects.equals(mark, sentence1.mark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentence, author, mark);
    }

}
