package net.thumbtack.school.elections.exception;

public class ElectionsException extends Exception {
    private ErrorCode errorCode;
    private String param;

    public ElectionsException(ErrorCode errorCode, String param) {
        this.errorCode = errorCode;
        this.param = param;
    }

    public ElectionsException(ErrorCode errorCode) {
        this(errorCode, null);
    }

    public ElectionsException(Throwable cause, ErrorCode errorCode) {
        super(cause);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        if (param != null)
            return String.format(errorCode.getMessage(), param);
        else
            return errorCode.getMessage();
    }
}
