package net.thumbtack.school.elections.exception;

public enum ErrorCode {
    SUCCESS(""),
    VOTER_NOT_FOUND("Voter not found"),
    SENTENCE_NOT_FOUND("Sentence not found"),
    CANDIDATE_NOT_FOUND("Candidate not found"),
    MARK_NOT_FOUND("Mark not found"),
    VOTER_ALREADY_EXISTS("Voter already exists"),
    SENTENCE_ALREADY_EXISTS("Sentence already exists"),
    CANDIDATE_ALREADY_EXISTS("Candidate already exists"),
    NULL_REQUEST("Null request"),
    CANNOT_CHANGE_MARK("Author cannot change mark"),
    CANNOT_REMOVE_MARK("Author cannot remove mark"),
    CANNOT_REMOVE_SENTENCE("Author cannot remove sentence"),
    METHOD_NOT_ALLOWED("Method not allowed"),
    UNKNOWN_ERROR("Unknown error"),
    CANNOT_LOGOUT_CANDIDATE("Cannot logout candidate, delete agreement first"),
    ELECTIONS_HAS_BEGUN("Elections has begun"),
    ELECTIONS_HAS_NOT_BEGUN("Elections has not begun"),
    CANNOT_VOTE_FOR_YOURSELF("Candidate cannot vote for yourself"),
    ELECTIONS_FAILED("Elections failed"),
    WRONG_PARAMS("Wrong params");

    private String message;

    ErrorCode(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
