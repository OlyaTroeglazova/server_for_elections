package net.thumbtack.school.elections.dao;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.model.Candidate;
import net.thumbtack.school.elections.model.Sentence;

import java.util.List;
import java.util.Map;

public interface SentenceDao {
    Sentence insertSentence(String firstName, String lastName, String sentence) throws ElectionsException;
    boolean addSentenceToCandidateProgram(String firstName, String lastName, String sentence) throws ElectionsException;
    boolean removeSentence(String firstName, String lastName, String sentence) throws ElectionsException;
    boolean addMark(String firstName, String lastName, String sentence, int mark) throws ElectionsException;
    boolean editMark(String firstName, String lastName, String sentence, int mark) throws ElectionsException;
    boolean removeMark(String firstName, String lastName, String sentence) throws ElectionsException;
    List<Sentence> getSentenceList(String firstName, String lastName) throws ElectionsException;
    Map<Candidate, List<Sentence>> getSentencesByCandidateList(String firstName, String lastName, String[] candidates) throws ElectionsException;
}
