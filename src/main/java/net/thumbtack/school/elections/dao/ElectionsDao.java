package net.thumbtack.school.elections.dao;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.model.Candidate;

public interface ElectionsDao {
    boolean startElections();
    Candidate stopElections() throws ElectionsException;

}
