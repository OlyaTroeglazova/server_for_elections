package net.thumbtack.school.elections.dao;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.model.Candidate;
import net.thumbtack.school.elections.model.Sentence;
import net.thumbtack.school.elections.model.Voter;

import java.util.List;

public interface VoterDao {

    Voter insertVoter(Voter voter) throws ElectionsException;
    Voter getVoterByName(String firstName, String lastName) throws ElectionsException;
    Voter loginVoter(String login, String password) throws ElectionsException;
    Voter logoutVoter(String firstName, String lastName) throws ElectionsException;
    boolean addVote(String voterFirstName, String voterLastName, String vote) throws ElectionsException;

}
