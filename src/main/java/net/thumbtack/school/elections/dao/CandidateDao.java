package net.thumbtack.school.elections.dao;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.model.Candidate;

import java.util.List;

public interface CandidateDao {
    Candidate insertCandidate(String firstNameVoter, String lastNameVoter, String firstNameCandidate, String lastNameCandidate) throws ElectionsException;
    boolean setAgreement(String firstName, String lastName) throws ElectionsException;
    boolean removeAgreement(String firstName, String lastName) throws ElectionsException;
    List<Candidate> getCandidatesList(String firstName, String lastName) throws ElectionsException;

}