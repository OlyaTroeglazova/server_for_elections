package net.thumbtack.school.elections.server;

import net.thumbtack.school.elections.database.DataBase;
import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.service.CandidateService;
import net.thumbtack.school.elections.service.ElectionsService;
import net.thumbtack.school.elections.service.SentenceService;
import net.thumbtack.school.elections.service.VoterService;

import java.io.*;

public class Server {
    private static VoterService voterService = new VoterService();
    private static SentenceService sentenceService = new SentenceService();
    private static CandidateService candidateService = new CandidateService();
    private static ElectionsService electionsService = new ElectionsService();
    private static DataBase dataBase;

    public static void startServer(String savedDataFileName) throws IOException {
        if(!new File(savedDataFileName).exists()){
            dataBase = DataBase.createDataBase();
        } else {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File(savedDataFileName)))) {
                dataBase = (DataBase) ois.readObject();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public static void stopServer(String saveDataFileName){
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File(saveDataFileName)))) {
            oos.writeObject(dataBase);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String registerVoter(String requestJsonString) throws ElectionsException {
        return voterService.insertVoter(requestJsonString);
    }

    public static String getVoter(String requestJsonString) throws ElectionsException {
        return voterService.getVoterByName(requestJsonString);
    }

    public static String loginVoter(String requestJsonString) throws ElectionsException {
        return voterService.loginVoter(requestJsonString);
    }

    public static String logoutVoter(String requestJsonString) throws ElectionsException {
        return voterService.logoutVoter(requestJsonString);
    }

    public static String insertSentence(String requestJsonString) throws ElectionsException {
        return sentenceService.insertSentence(requestJsonString);
    }

    public static String addSentenceToCandidateProgram(String requestJsonString) throws ElectionsException {
        return sentenceService.addSentenceToCandidateProgram(requestJsonString);
    }

    public static String removeSentence(String requestJsonString) throws ElectionsException {
        return sentenceService.removeSentence(requestJsonString);
    }

    public static String addSentenceMark(String requestJsonString) throws ElectionsException {
        return sentenceService.addMark(requestJsonString);
    }

    public static String editSentenceMark(String requestJsonString) throws ElectionsException {
        return sentenceService.editMark(requestJsonString);
    }

    public static String removeSentenceMark(String requestJsonString) throws ElectionsException {
        return sentenceService.removeMark(requestJsonString);
    }

    public static String registerCandidate(String requestJsonString) throws ElectionsException {
        return candidateService.insertCandidate(requestJsonString);
    }

    public static String setAgreementToNominate(String requestJsonString) throws ElectionsException {
        return candidateService.setAgreement(requestJsonString);
    }

    public static String removeAgreementToNominate(String requestJsonString) throws ElectionsException {
        return candidateService.removeAgreement(requestJsonString);
    }

    public static String getCandidatesList(String requestJsonString) throws ElectionsException {
        return candidateService.getCandidatesList(requestJsonString);
    }

    public static String getSentenceList(String requestJsonString) throws ElectionsException {
        return sentenceService.getSentenceList(requestJsonString);
    }

    public static String getSentencesByCandidateList(String requestJsonString) throws ElectionsException {
        return sentenceService.getSentencesByCandidateList(requestJsonString);
    }

    public static String startElections(String requestJsonString) throws ElectionsException {
        return electionsService.startElections(requestJsonString);
    }

    public static String stopElections(String requestJsonString) throws ElectionsException {
        return electionsService.stopElections(requestJsonString);
    }

    public static String addVote(String requestJsonString) throws ElectionsException {
        return voterService.addVote(requestJsonString);
    }

}
