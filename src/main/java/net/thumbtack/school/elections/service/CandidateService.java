package net.thumbtack.school.elections.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.thumbtack.school.elections.dao.CandidateDao;
import net.thumbtack.school.elections.daoimpl.CandidateDaoImpl;
import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.model.Candidate;
import net.thumbtack.school.elections.request.GetCandidatesListDtoRequest;
import net.thumbtack.school.elections.request.RegisterCandidateDtoRequest;
import net.thumbtack.school.elections.request.RemoveAgreementToNominateDtoRequest;
import net.thumbtack.school.elections.request.SetAgreementToNominateDtoRequest;
import net.thumbtack.school.elections.response.GetCandidatesListDtoResponse;
import net.thumbtack.school.elections.response.RegisterCandidateDtoResponse;
import net.thumbtack.school.elections.response.RemoveAgreementToNominateDtoResponse;
import net.thumbtack.school.elections.response.SetAgreementToNominateDtoResponse;

import java.util.List;

public class CandidateService {
    private static final Gson gson = new GsonBuilder().create();
    private CandidateDao candidateDAO = new CandidateDaoImpl();

    public String insertCandidate(String json) throws ElectionsException {
        try {
            RegisterCandidateDtoRequest request = gson.fromJson(json, RegisterCandidateDtoRequest.class);
            request.validate();
            candidateDAO.insertCandidate(request.getFirstNameVoter(), request.getLastNameVoter(),
                    request.getFirstNameCandidate(), request.getLastNameCandidate());
            RegisterCandidateDtoResponse response = new RegisterCandidateDtoResponse();
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            RegisterCandidateDtoResponse response = new RegisterCandidateDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String setAgreement(String json) throws ElectionsException {
        try {
            SetAgreementToNominateDtoRequest request = gson.fromJson(json, SetAgreementToNominateDtoRequest.class);
            request.validate();
            candidateDAO.setAgreement(request.getFirstName(), request.getLastName());
            SetAgreementToNominateDtoResponse response = new SetAgreementToNominateDtoResponse();
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            SetAgreementToNominateDtoResponse response = new SetAgreementToNominateDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String removeAgreement(String json) throws ElectionsException {
        try {
            RemoveAgreementToNominateDtoRequest request = gson.fromJson(json, RemoveAgreementToNominateDtoRequest.class);
            request.validate();
            candidateDAO.removeAgreement(request.getFirstName(), request.getLastName());
            RemoveAgreementToNominateDtoResponse response = new RemoveAgreementToNominateDtoResponse();
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            RemoveAgreementToNominateDtoResponse response = new RemoveAgreementToNominateDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String getCandidatesList(String json) throws ElectionsException {
        try {
            GetCandidatesListDtoRequest request = gson.fromJson(json, GetCandidatesListDtoRequest.class);
            request.validate();
            List<Candidate> item = candidateDAO.getCandidatesList(request.getFirstName(), request.getLastName());
            GetCandidatesListDtoResponse response = new GetCandidatesListDtoResponse(item);
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            GetCandidatesListDtoResponse response = new GetCandidatesListDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }
}
