package net.thumbtack.school.elections.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.thumbtack.school.elections.dao.VoterDao;
import net.thumbtack.school.elections.daoimpl.VoterDaoImpl;
import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.model.Voter;
import net.thumbtack.school.elections.request.*;
import net.thumbtack.school.elections.response.*;

public class VoterService {
    private static final Gson gson = new GsonBuilder().create();
    private VoterDao voterDAO = new VoterDaoImpl();

    public String insertVoter(String json) throws ElectionsException {
        try {
            RegisterVoterDtoRequest request = gson.fromJson(json, RegisterVoterDtoRequest.class);
            request.validate();
            Voter item = new Voter(request.getFirstName(), request.getLastName(), request.getPatronymic(),
                    request.getStreet(), request.getHomeNumber(), request.getApartmentNumber(), request.getLogin(), request.getPassword());
            Voter addItem = voterDAO.insertVoter(item);
            RegisterVoterDtoResponse response = new RegisterVoterDtoResponse(addItem.getToken());
            return gson.toJson(response);
        } catch (ElectionsException ex) {
            RegisterVoterDtoResponse response = new RegisterVoterDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String getVoterByName(String json) throws ElectionsException {
        try {
            GetVoterDtoRequest request = gson.fromJson(json, GetVoterDtoRequest.class);
            request.validate();
            Voter item = voterDAO.getVoterByName(request.getFirstName(), request.getLastName());
            GetVoterDtoResponse response = new GetVoterDtoResponse(item.getToken(), item.getFirstName(), item.getLastName(), item.getPatronymic(),
                    item.getStreet(), item.getHomeNumber(), item.getApartmentNumber(), item.getLogin(), item.getPassword());
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            GetVoterDtoResponse response = new GetVoterDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String loginVoter(String json) throws ElectionsException {
        try {
            LoginVoterDtoRequest request = gson.fromJson(json, LoginVoterDtoRequest.class);
            request.validate();
            Voter voter = voterDAO.loginVoter(request.getLogin(), request.getPassword());
            LoginVoterDtoResponse response = new LoginVoterDtoResponse(voter.getToken());
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            LoginVoterDtoResponse response = new LoginVoterDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String logoutVoter(String json) throws ElectionsException {
        try {
            LogoutVoterDtoRequest request = gson.fromJson(json, LogoutVoterDtoRequest.class);
            request.validate();
            Voter item = voterDAO.logoutVoter(request.getFirstName(), request.getLastName());
            LogoutVoterDtoResponse response = new LogoutVoterDtoResponse();
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            LogoutVoterDtoResponse response = new LogoutVoterDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String addVote(String json) throws ElectionsException {
        try {
            AddVoteDtoRequest request = gson.fromJson(json, AddVoteDtoRequest.class);
            request.validate();
            voterDAO.addVote(request.getVoterFirstName(), request.getVoterLastName(), request.getVote());
            AddVoteDtoResponse response = new AddVoteDtoResponse();
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            AddVoteDtoResponse response = new AddVoteDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }
}