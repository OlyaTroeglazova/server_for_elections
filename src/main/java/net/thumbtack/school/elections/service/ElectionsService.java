package net.thumbtack.school.elections.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.thumbtack.school.elections.dao.ElectionsDao;
import net.thumbtack.school.elections.daoimpl.ElectionsDaoImpl;
import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.model.Candidate;
import net.thumbtack.school.elections.request.RegisterCandidateDtoRequest;
import net.thumbtack.school.elections.request.StartElectionsDtoRequest;
import net.thumbtack.school.elections.request.StopElectionsDtoRequest;
import net.thumbtack.school.elections.response.RegisterCandidateDtoResponse;
import net.thumbtack.school.elections.response.StartElectionsDtoResponse;
import net.thumbtack.school.elections.response.StopElectionsDtoResponse;

public class ElectionsService {

    private static final Gson gson = new GsonBuilder().create();
    private ElectionsDao electionsDao = new ElectionsDaoImpl();

    public String startElections(String json) throws ElectionsException {
        StartElectionsDtoRequest request = gson.fromJson(json, StartElectionsDtoRequest.class);
        electionsDao.startElections();
        StartElectionsDtoResponse response = new StartElectionsDtoResponse();
        return gson.toJson(response);
    }

    public String stopElections(String json) throws ElectionsException {
        try {
            StopElectionsDtoRequest request = gson.fromJson(json, StopElectionsDtoRequest.class);
            Candidate item = electionsDao.stopElections();
            StopElectionsDtoResponse response = new StopElectionsDtoResponse(item.getVoter().getFirstName(), item.getVoter().getLastName());
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            StopElectionsDtoResponse response = new StopElectionsDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

}
