package net.thumbtack.school.elections.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.thumbtack.school.elections.dao.SentenceDao;
import net.thumbtack.school.elections.daoimpl.SentenceDaoImpl;
import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.model.Candidate;
import net.thumbtack.school.elections.model.Sentence;
import net.thumbtack.school.elections.request.*;
import net.thumbtack.school.elections.response.*;

import java.util.List;
import java.util.Map;

public class SentenceService {
    private static final Gson gson = new GsonBuilder().create();
    private SentenceDao sentenceDAO = new SentenceDaoImpl();

    public String insertSentence(String json) throws ElectionsException {
        try {
            InsertSentenceDtoRequest request = gson.fromJson(json, InsertSentenceDtoRequest.class);
            request.validate();
            Sentence item = sentenceDAO.insertSentence(request.getFirstName(), request.getLastName(), request.getSentence());
            InsertSentenceDtoResponse response = new InsertSentenceDtoResponse(item.getAuthor(), item.getSentence());
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            InsertSentenceDtoResponse response = new InsertSentenceDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String addSentenceToCandidateProgram(String json) throws ElectionsException {
        try {
            AddSentenceToCandidateProgramDtoRequest request = gson.fromJson(json, AddSentenceToCandidateProgramDtoRequest.class);
            request.validate();
            sentenceDAO.addSentenceToCandidateProgram(request.getFirstName(), request.getLastName(), request.getSentence());
            AddSentenceToCandidateProgramDtoResponse response = new AddSentenceToCandidateProgramDtoResponse();
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            AddSentenceToCandidateProgramDtoResponse response = new AddSentenceToCandidateProgramDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String removeSentence(String json) throws ElectionsException {
        try {
            RemoveSentenceDtoRequest request = gson.fromJson(json, RemoveSentenceDtoRequest.class);
            request.validate();
            sentenceDAO.removeSentence(request.getFirstName(), request.getLastName(), request.getSentence());
            RemoveSentenceDtoResponse response = new RemoveSentenceDtoResponse();
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            RemoveSentenceDtoResponse response = new RemoveSentenceDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String addMark(String json) throws ElectionsException {
        try {
            AddSentenceMarkDtoRequest request = gson.fromJson(json, AddSentenceMarkDtoRequest.class);
            request.validate();
            sentenceDAO.addMark(request.getFirstName(), request.getLastName(), request.getSentence(), request.getMark());
            AddSentenceMarkDtoResponse response = new AddSentenceMarkDtoResponse();
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            AddSentenceMarkDtoResponse response = new AddSentenceMarkDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String editMark(String json) throws ElectionsException {
        try {
            EditSentenceMarkDtoRequest request = gson.fromJson(json, EditSentenceMarkDtoRequest.class);
            request.validate();
            sentenceDAO.editMark(request.getFirstName(), request.getLastName(), request.getSentence(), request.getMark());
            EditSentenceMarkDtoResponse response = new EditSentenceMarkDtoResponse();
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            EditSentenceMarkDtoResponse response = new EditSentenceMarkDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String removeMark(String json) throws ElectionsException {
        try {
            RemoveSentenceMarkDtoRequest request = gson.fromJson(json, RemoveSentenceMarkDtoRequest.class);
            request.validate();
            sentenceDAO.removeMark(request.getFirstName(), request.getLastName(), request.getSentence());
            RemoveSentenceMarkDtoResponse response = new RemoveSentenceMarkDtoResponse();
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            RemoveSentenceMarkDtoResponse response = new RemoveSentenceMarkDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String getSentenceList(String json) throws ElectionsException {
        try {
            GetSentenceListDtoRequest request = gson.fromJson(json, GetSentenceListDtoRequest.class);
            request.validate();
            List<Sentence> item = sentenceDAO.getSentenceList(request.getFirstName(), request.getLastName());
            GetSentenceListDtoResponse response = new GetSentenceListDtoResponse(item);
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            GetSentenceListDtoResponse response = new GetSentenceListDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }

    public String getSentencesByCandidateList(String json) throws ElectionsException {
        try {
            GetSentencesByCandidateListDeoRequest request = gson.fromJson(json, GetSentencesByCandidateListDeoRequest.class);
            request.validate();
            Map<Candidate, List<Sentence>> item = sentenceDAO.getSentencesByCandidateList(request.getFirstName(), request.getLastName(), request.getCandidates());
            GetSentencesByCandidateListDeoResponse response = new GetSentencesByCandidateListDeoResponse(item);
            return gson.toJson(response);
        }
        catch (ElectionsException ex) {
            GetSentenceListDtoResponse response = new GetSentenceListDtoResponse(ex.getErrorCode());
            return gson.toJson(response);
        }
    }
}
