package net.thumbtack.school.elections.daoimpl;

import net.thumbtack.school.elections.dao.VoterDao;
import net.thumbtack.school.elections.database.DataBase;
import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.model.Voter;

public class VoterDaoImpl implements VoterDao {

    private static DataBase dataBase = DataBase.createDataBase();

    @Override
    public Voter insertVoter(Voter voter) throws ElectionsException {
        return dataBase.insertVoter(voter);
    }

    @Override
    public Voter getVoterByName(String firstName, String lastName) throws ElectionsException {
        return dataBase.getVoterByName(firstName, lastName);
    }

    @Override
    public Voter loginVoter(String login, String password) throws ElectionsException {
        return dataBase.login(login, password);
    }

    @Override
    public Voter logoutVoter(String firstName, String lastName) throws ElectionsException {
        return dataBase.logout(firstName, lastName);
    }

    @Override
    public boolean addVote(String voterFirstName, String voterLastName, String vote) throws ElectionsException {
        return dataBase.addVote(voterFirstName, voterLastName, vote);
    }


}
