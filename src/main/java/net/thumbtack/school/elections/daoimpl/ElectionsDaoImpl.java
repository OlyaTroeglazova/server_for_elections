package net.thumbtack.school.elections.daoimpl;

import net.thumbtack.school.elections.dao.ElectionsDao;
import net.thumbtack.school.elections.database.DataBase;
import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.model.Candidate;

public class ElectionsDaoImpl implements ElectionsDao {

    private static DataBase dataBase = DataBase.createDataBase();


    @Override
    public boolean startElections() {
        return dataBase.startElections();
    }

    @Override
    public Candidate stopElections() throws ElectionsException {
        return dataBase.stopElections();
    }

}
