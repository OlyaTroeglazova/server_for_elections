package net.thumbtack.school.elections.daoimpl;

import net.thumbtack.school.elections.dao.SentenceDao;
import net.thumbtack.school.elections.database.DataBase;
import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.model.Candidate;
import net.thumbtack.school.elections.model.Sentence;

import java.util.List;
import java.util.Map;

public class SentenceDaoImpl implements SentenceDao {
    private static DataBase dataBase = DataBase.createDataBase();

    @Override
    public Sentence insertSentence(String firstName, String lastName, String sentence) throws ElectionsException {
        return dataBase.insertSentence(firstName, lastName, sentence);
    }

    @Override
    public boolean addSentenceToCandidateProgram(String firstName, String lastName, String sentence) throws ElectionsException {
        return dataBase.addSentenceToCandidateProgram(firstName, lastName, sentence);
    }

    @Override
    public boolean removeSentence(String firstName, String lastName, String sentence) throws ElectionsException {
        return dataBase.removeSentence(firstName, lastName, sentence);
    }

    @Override
    public boolean addMark(String firstName, String lastName, String sentence, int mark) throws ElectionsException {
        return dataBase.addSentenceMark(firstName, lastName, sentence, mark);
    }

    @Override
    public boolean editMark(String firstName, String lastName, String sentence, int mark) throws ElectionsException {
        return dataBase.editSentenceMark(firstName, lastName, sentence, mark);
    }

    @Override
    public boolean removeMark(String firstName, String lastName, String sentence) throws ElectionsException {
        return dataBase.removeSentenceMark(firstName, lastName, sentence);
    }

    @Override
    public List<Sentence> getSentenceList(String firstName, String lastName) throws ElectionsException {
        return dataBase.getSentenceList(firstName, lastName);
    }

    @Override
    public Map<Candidate, List<Sentence>> getSentencesByCandidateList(String firstName, String lastName, String[] candidates) throws ElectionsException {
        return dataBase.getSentencesByCandidateList(firstName, lastName, candidates);
    }

}
