package net.thumbtack.school.elections.daoimpl;

import net.thumbtack.school.elections.dao.CandidateDao;
import net.thumbtack.school.elections.database.DataBase;
import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.model.Candidate;

import java.util.List;

public class CandidateDaoImpl implements CandidateDao {
    private static DataBase dataBase = DataBase.createDataBase();

    @Override
    public Candidate insertCandidate(String firstNameVoter, String lastNameVoter, String firstNameCandidate, String lastNameCandidate) throws ElectionsException {
        return dataBase.insertCandidate(firstNameVoter, lastNameVoter, firstNameCandidate, lastNameCandidate);
    }

    @Override
    public boolean setAgreement(String firstName, String lastName) throws ElectionsException{
        return dataBase.setAgreement(firstName, lastName);
    }

    @Override
    public boolean removeAgreement(String firstName, String lastName) throws ElectionsException {
        return dataBase.removeAgreement(firstName, lastName);
    }

    @Override
    public List<Candidate> getCandidatesList(String firstName, String lastName) throws ElectionsException {
        return dataBase.getCandidatesList(firstName, lastName);
    }
}
