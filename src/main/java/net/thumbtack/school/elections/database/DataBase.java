package net.thumbtack.school.elections.database;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.exception.ErrorCode;
import net.thumbtack.school.elections.model.Candidate;
import net.thumbtack.school.elections.model.Mark;
import net.thumbtack.school.elections.model.Sentence;
import net.thumbtack.school.elections.model.Voter;

import java.io.Serializable;
import java.util.*;

public class DataBase implements Serializable {
    private static DataBase dataBase;
    private static Map<String, Voter> voters;
    private static Map<String, Sentence> sentences;
    private static Map<String, Candidate> candidates;
    private static boolean elections;
    private static Map<String, String> votes;

    private DataBase(){
        voters = new HashMap<>();
        sentences = new HashMap<>();
        candidates = new HashMap<>();
        elections = false;
        votes = new HashMap<>();
    }

    public static DataBase createDataBase(){
        if (dataBase==null){
            dataBase = new DataBase();
        }
        return dataBase;
    }

    public Voter insertVoter(Voter voter) throws ElectionsException{
        if(elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_BEGUN);
        }
        if(voters.containsValue(voter)){
            throw new ElectionsException(ErrorCode.VOTER_ALREADY_EXISTS);
        }
        for(Voter v : voters.values()){
            if(v.getLogin().equals(voter.getLogin()) ||
                    (v.getFirstName().equals(voter.getFirstName()) && v.getLastName().equals(voter.getLastName()) && v.getPatronymic().equals(voter.getPatronymic()))){
                throw new ElectionsException(ErrorCode.VOTER_ALREADY_EXISTS);
            }
        }
        String name = voter.getFirstName() + voter.getLastName();
        voter.login();
        voters.put(name, voter);
        return voter;
    }

    public Voter getVoterByName(String firstName, String lastName) throws ElectionsException {
        if (!voters.containsKey(firstName+lastName) || voters.get(firstName + lastName).getToken()==null){
            throw new ElectionsException((ErrorCode.VOTER_NOT_FOUND));
        }
        return voters.get(firstName + lastName);
    }

    public Candidate getCandidateByName(String firstName, String lastName) throws ElectionsException {
        if (!candidates.containsKey(firstName+lastName) || candidates.get(firstName + lastName).getVoter().getToken()==null){
            throw new ElectionsException((ErrorCode.CANDIDATE_NOT_FOUND));
        }
        return candidates.get(firstName + lastName);
    }

    public Voter login(String login, String password) throws ElectionsException {
        if(elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_BEGUN);
        }
        for(Voter v : voters.values()){
            if(v.getLogin().equals(login) && v.getPassword().equals(password)){
                if(v.getToken()!=null)
                    throw new ElectionsException(ErrorCode.VOTER_ALREADY_EXISTS);
                v.login();
                return v;
            }
        }
        throw new ElectionsException(ErrorCode.VOTER_NOT_FOUND);
    }

    public Voter logout(String firstName, String lastName) throws ElectionsException {
        if(elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_BEGUN);
        }
        if (!voters.containsKey(firstName+lastName) || voters.get(firstName + lastName).getToken()==null){
            throw new ElectionsException((ErrorCode.VOTER_NOT_FOUND));
        }
        if(candidates.containsKey(firstName+lastName)) {
            if (candidates.get(firstName + lastName).isAgreement()) {
                throw new ElectionsException(ErrorCode.CANNOT_LOGOUT_CANDIDATE);
            }
        }
        for(String s: sentences.keySet()){
            if(s.equals(firstName+lastName)){
                Sentence sentence = sentences.get(firstName+lastName);
                sentences.remove(firstName+lastName);
                sentences.put("Сommunity of city dwellers", sentence);
            }
        }
        voters.get(firstName + lastName).logout();
        return voters.get(firstName+lastName);
    }

    //избиратель высказывает предложение
    public Sentence insertSentence(String firstName, String lastName, String sentence) throws ElectionsException {
        if(elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_BEGUN);
        }
        if(sentences.containsKey(sentence)){
            throw new ElectionsException(ErrorCode.SENTENCE_ALREADY_EXISTS);
        }
        Sentence s = new Sentence(getVoterByName(firstName, lastName), sentence);
        sentences.put(sentence, s);
        if(candidates.containsKey(firstName+lastName)){
            candidates.get(firstName+lastName).setSentence(s);
        }
        return s;
    }

    //кандидат добавляет в свою программу предложения избирателей или других кандидатов
    public boolean addSentenceToCandidateProgram(String firstName, String lastName, String sentence) throws ElectionsException {
        if(elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_BEGUN);
        }
        if (!sentences.containsKey(sentence)) {
            throw new ElectionsException(ErrorCode.SENTENCE_NOT_FOUND);
        }
        Sentence s = sentences.get(sentence);
        getCandidateByName(firstName, lastName).setSentence(s);
        return true;
    }

    //кандидат удаляет предложение
    public boolean removeSentence(String firstName, String lastName, String sentence) throws ElectionsException {
        if(elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_BEGUN);
        }
        if (!sentences.containsKey(sentence)) {
            throw new ElectionsException(ErrorCode.SENTENCE_NOT_FOUND);
        }
        if (sentences.get(sentence).getAuthor().equals(getCandidateByName(firstName, lastName))) {
            throw new ElectionsException(ErrorCode.CANNOT_REMOVE_SENTENCE);
        }
        getCandidateByName(firstName, lastName).getProgram().remove(sentences.get(sentence));
        return true;
    }

    public boolean addSentenceMark(String firstName, String lastName, String sentence, int mark) throws ElectionsException{
        if(elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_BEGUN);
        }
        if (!sentences.containsKey(sentence)){
            throw new ElectionsException(ErrorCode.SENTENCE_NOT_FOUND);
        }
        sentences.get(sentence).setMark(getVoterByName(firstName, lastName), mark);
        return true;
    }

    public boolean editSentenceMark(String firstName, String lastName, String sentence, int mark) throws ElectionsException{
        if(elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_BEGUN);
        }
        if (!sentences.containsKey(sentence)){
            throw new ElectionsException(ErrorCode.SENTENCE_NOT_FOUND);
        }
        Sentence s = sentences.get(sentence);
        if(s.getAuthor().getFirstName().equals(firstName) && s.getAuthor().getLastName().equals(lastName)){
            throw new ElectionsException(ErrorCode.CANNOT_CHANGE_MARK);
        }
        List<Mark> marks = s.getMarkList();
        for(Mark m: marks){
            if(m.getAuthor().getFirstName().equals(firstName) && m.getAuthor().getLastName().equals(lastName)){
                s.setMark(getVoterByName(firstName, lastName), mark);
                return true;
            }
        }
        throw new ElectionsException(ErrorCode.MARK_NOT_FOUND);
    }

    public boolean removeSentenceMark(String firstName, String lastName, String sentence) throws ElectionsException{
        if(elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_BEGUN);
        }
        if (!sentences.containsKey(sentence)){
            throw new ElectionsException(ErrorCode.SENTENCE_NOT_FOUND);
        }
        Sentence s = sentences.get(sentence);
        if(s.getAuthor().getFirstName().equals(firstName) && s.getAuthor().getLastName().equals(lastName)){
            throw new ElectionsException(ErrorCode.CANNOT_REMOVE_MARK);
        }
        List<Mark> marks = s.getMarkList();
        for(Mark m: marks){
            if(m.getAuthor().getFirstName().equals(firstName) && m.getAuthor().getLastName().equals(lastName)){
                s.removeMark(getVoterByName(firstName, lastName));
                return true;
            }
        }
        throw new ElectionsException(ErrorCode.MARK_NOT_FOUND);
    }

    public Candidate insertCandidate(String firstNameVoter, String lastNameVoter,
                                     String firstNameCandidate, String lastNameCandidate) throws ElectionsException{
        if(elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_BEGUN);
        }
        if(candidates.containsKey(firstNameCandidate+lastNameCandidate)){
            throw new ElectionsException(ErrorCode.CANDIDATE_ALREADY_EXISTS);
        }
        Voter voter = getVoterByName(firstNameVoter, lastNameVoter);
        Candidate candidate = new Candidate(getVoterByName(firstNameCandidate, lastNameCandidate));
        if(voter.equals(candidate.getVoter())){
            candidate.setAgreement(true);
        }
        candidates.put(firstNameCandidate+lastNameCandidate, candidate);
        for(Sentence sentence: sentences.values()){
            if(sentence.getAuthor().equals(candidate.getVoter())){
                candidate.setSentence(sentence);
            }
        }
        return candidate;
    }

    public boolean setAgreement(String firstName, String lastName) throws ElectionsException {
        if(elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_BEGUN);
        }
        if(!candidates.containsKey(firstName+lastName)){
            throw new ElectionsException(ErrorCode.CANDIDATE_NOT_FOUND);
        }
        candidates.get(firstName+lastName).setAgreement(true);
        return true;
    }

    public boolean removeAgreement(String firstName, String lastName) throws ElectionsException {
        if(elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_BEGUN);
        }
        if(!candidates.containsKey(firstName+lastName)){
            throw new ElectionsException(ErrorCode.CANDIDATE_NOT_FOUND);
        }
        candidates.get(firstName+lastName).setAgreement(false);
        return true;
    }

    public List<Candidate> getCandidatesList(String firstName, String lastName) throws ElectionsException {
        getVoterByName(firstName, lastName);
        return new ArrayList<>(candidates.values());
    }

    public List<Sentence> getSentenceList(String firstName, String lastName) throws ElectionsException {
        getVoterByName(firstName, lastName);
        return new ArrayList<>(sentences.values());
    }

    public Map<Candidate, List<Sentence>> getSentencesByCandidateList(String firstName, String lastName, String[] names) throws ElectionsException {
        getVoterByName(firstName, lastName);
        Map<Candidate, List<Sentence>> sentences = new HashMap<>();
        for(int i = 0; i<names.length-1; i++){
            Candidate c = getCandidateByName(names[i], names[i+1]);
            sentences.put(c, c.getProgram());
        }
        return sentences;
    }

    public boolean startElections(){
        elections = true;
        return true;
    }

    public boolean addVote(String voterFirstName, String voterLastName, String vote) throws ElectionsException {
        if(!elections){
            throw new ElectionsException(ErrorCode.ELECTIONS_HAS_NOT_BEGUN);
        }
        getVoterByName(voterFirstName, voterLastName);
        if(vote.equals("Against all")){
            votes.put(voterFirstName+voterLastName, vote);
            return true;
        }
        if (!candidates.containsKey(vote) || candidates.get(vote).getVoter().getToken()==null){
            throw new ElectionsException((ErrorCode.CANDIDATE_NOT_FOUND));
        }
        if(candidates.get(vote).equals(candidates.get(voterFirstName+voterLastName))){
            throw new ElectionsException(ErrorCode.CANNOT_VOTE_FOR_YOURSELF);
        }
        votes.put(voterFirstName + voterLastName, vote);
        return true;
    }

    public Candidate stopElections() throws ElectionsException {
        Map<String, Integer> result = new HashMap<>();
        elections = false;
        for(String s: votes.values()){
            if(!result.containsKey(s)){
                result.put(s, 1);
            }
            else {
                result.put(s, result.get(s)+1);
            }
        }
        int max=0;
        String winner = "";
        for(String s: result.keySet()){
            if(result.get(s)>max){
                max = result.get(s);
                winner = s;
            }
        }
        if(winner.equals("Against all")){
            throw new ElectionsException(ErrorCode.ELECTIONS_FAILED);
        }
        return candidates.get(winner);
    }

}