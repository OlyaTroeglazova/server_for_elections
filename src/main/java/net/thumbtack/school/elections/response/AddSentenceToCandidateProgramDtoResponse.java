package net.thumbtack.school.elections.response;

import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class AddSentenceToCandidateProgramDtoResponse {
    private ErrorCode errorCode;

    public AddSentenceToCandidateProgramDtoResponse() {
        this.errorCode = ErrorCode.SUCCESS;
    }

    public AddSentenceToCandidateProgramDtoResponse(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddSentenceToCandidateProgramDtoResponse that = (AddSentenceToCandidateProgramDtoResponse) o;
        return errorCode == that.errorCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(errorCode);
    }
}
