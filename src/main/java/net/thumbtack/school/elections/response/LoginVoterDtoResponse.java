package net.thumbtack.school.elections.response;

import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class LoginVoterDtoResponse {
    private String token;
    private ErrorCode error;

    public LoginVoterDtoResponse(String token) {
        this.token = token;
        this.error = ErrorCode.SUCCESS;
    }

    public LoginVoterDtoResponse(ErrorCode error) {
        this.error = error;
    }

    public String getToken() {
        return token;
    }

    public ErrorCode getErrorCode() {
        return error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginVoterDtoResponse that = (LoginVoterDtoResponse) o;
        return Objects.equals(token, that.token) &&
                error == that.error;
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, error);
    }
}
