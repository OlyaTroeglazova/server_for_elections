package net.thumbtack.school.elections.response;

import net.thumbtack.school.elections.exception.ErrorCode;
import net.thumbtack.school.elections.model.Candidate;
import net.thumbtack.school.elections.model.Sentence;

import java.util.*;

public class GetCandidatesListDtoResponse {
    private Map<String, List<String>> candidates;
    private ErrorCode errorCode;

    public GetCandidatesListDtoResponse(List<Candidate> candidates) {
        this.candidates = new HashMap<>();
        for(Candidate c : candidates){
            List<String> program = new ArrayList<>();
            for(Sentence s: c.getProgram()){
                program.add(s.getSentence());
            }
            this.candidates.put(c.getVoter().getFirstName()+c.getVoter().getLastName(), program);
        }
        this.errorCode = ErrorCode.SUCCESS;
    }

    public GetCandidatesListDtoResponse(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public Map<String, List<String>> getCandidates() {
        return candidates;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetCandidatesListDtoResponse that = (GetCandidatesListDtoResponse) o;
        return Objects.equals(candidates, that.candidates) &&
                errorCode == that.errorCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(candidates, errorCode);
    }
}