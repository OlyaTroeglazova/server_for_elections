package net.thumbtack.school.elections.response;

import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class GetVoterDtoResponse {
    private String token;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String street;
    private String homeNumber;
    private String apartmentNumber;
    private String login;
    private String password;
    private ErrorCode errorCode;

    public GetVoterDtoResponse(String token, String firstName, String lastName, String patronymic, String street,
                 String homeNumber, String apartmentNumber, String login, String password) {
        this.token = token;
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.street = street;
        this.homeNumber = homeNumber;
        this.apartmentNumber = apartmentNumber;
        this.login = login;
        this.password = password;
        this.errorCode = ErrorCode.SUCCESS;
    }

    public GetVoterDtoResponse(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getToken() {
        return token;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getStreet() {
        return street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetVoterDtoResponse response = (GetVoterDtoResponse) o;
        return Objects.equals(token, response.token) &&
                Objects.equals(firstName, response.firstName) &&
                Objects.equals(lastName, response.lastName) &&
                Objects.equals(patronymic, response.patronymic) &&
                Objects.equals(street, response.street) &&
                Objects.equals(homeNumber, response.homeNumber) &&
                Objects.equals(apartmentNumber, response.apartmentNumber) &&
                Objects.equals(login, response.login) &&
                Objects.equals(password, response.password) &&
                errorCode == response.errorCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, firstName, lastName, patronymic, street, homeNumber, apartmentNumber, login, password, errorCode);
    }
}
