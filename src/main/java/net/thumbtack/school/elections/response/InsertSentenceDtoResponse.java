package net.thumbtack.school.elections.response;

import net.thumbtack.school.elections.exception.ErrorCode;
import net.thumbtack.school.elections.model.Voter;

import java.util.Objects;

public class InsertSentenceDtoResponse {

    private String sentence;
    private Voter author;
    private ErrorCode errorCode;

    public InsertSentenceDtoResponse(Voter author, String sentence) {
        this.sentence = sentence;
        this.author = author;
        this.errorCode = ErrorCode.SUCCESS;
    }

    public InsertSentenceDtoResponse() {
        this.errorCode = ErrorCode.SUCCESS;
    }

    public InsertSentenceDtoResponse(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getSentence() {
        return sentence;
    }

    public Voter getAuthor() {
        return author;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InsertSentenceDtoResponse response = (InsertSentenceDtoResponse) o;
        return Objects.equals(sentence, response.sentence) &&
                Objects.equals(author, response.author) &&
                errorCode == response.errorCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentence, author, errorCode);
    }
}
