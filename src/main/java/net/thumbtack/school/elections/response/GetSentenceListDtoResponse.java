package net.thumbtack.school.elections.response;

import net.thumbtack.school.elections.exception.ErrorCode;
import net.thumbtack.school.elections.model.Sentence;

import java.util.*;

public class GetSentenceListDtoResponse {
    private Map<String, Double> sentences;
    private ErrorCode errorCode;

    public GetSentenceListDtoResponse(List<Sentence> sentences) {
        this.sentences = new HashMap<>();
        Map<String, Double> map = new HashMap<>();
        for(Sentence s : sentences){
            map.put(s.getSentence(), s.getMark());
        }
        map.entrySet().stream().sorted(Map.Entry.<String, Double>comparingByValue().reversed()).forEach(s -> this.sentences.put(s.getKey(), s.getValue()));
        this.errorCode = ErrorCode.SUCCESS;
    }

    public GetSentenceListDtoResponse(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetSentenceListDtoResponse that = (GetSentenceListDtoResponse) o;
        return Objects.equals(sentences, that.sentences) &&
                errorCode == that.errorCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentences, errorCode);
    }
}
