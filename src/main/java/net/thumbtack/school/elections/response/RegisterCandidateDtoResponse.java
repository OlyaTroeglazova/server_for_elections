package net.thumbtack.school.elections.response;

import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class RegisterCandidateDtoResponse {
    private ErrorCode errorCode;

    public RegisterCandidateDtoResponse() {
        this.errorCode = ErrorCode.SUCCESS;
    }

    public RegisterCandidateDtoResponse(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisterCandidateDtoResponse response = (RegisterCandidateDtoResponse) o;
        return errorCode == response.errorCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(errorCode);
    }
}
