package net.thumbtack.school.elections.response;

import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class AddSentenceMarkDtoResponse {
    private ErrorCode errorCode;

    public AddSentenceMarkDtoResponse() {
        this.errorCode = ErrorCode.SUCCESS;
    }

    public AddSentenceMarkDtoResponse(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddSentenceMarkDtoResponse that = (AddSentenceMarkDtoResponse) o;
        return errorCode == that.errorCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(errorCode);
    }
}
