package net.thumbtack.school.elections.response;

import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class StopElectionsDtoResponse {
    private String firstName;
    private String lastName;
    private ErrorCode errorCode;

    public StopElectionsDtoResponse(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.errorCode = ErrorCode.SUCCESS;
    }

    public StopElectionsDtoResponse(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StopElectionsDtoResponse that = (StopElectionsDtoResponse) o;
        return Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                errorCode == that.errorCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, errorCode);
    }
}
