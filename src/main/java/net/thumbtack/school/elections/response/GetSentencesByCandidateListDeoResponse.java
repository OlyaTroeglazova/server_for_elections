package net.thumbtack.school.elections.response;

import net.thumbtack.school.elections.exception.ErrorCode;
import net.thumbtack.school.elections.model.Candidate;
import net.thumbtack.school.elections.model.Sentence;

import java.util.*;

public class GetSentencesByCandidateListDeoResponse {
    private Map<String, List<String>> sentences;
    private ErrorCode errorCode;

    public GetSentencesByCandidateListDeoResponse(Map<Candidate, List<Sentence>> sentences) {
        this.sentences = new HashMap<>();
        for(Map.Entry<Candidate, List<Sentence>> e: sentences.entrySet()){
            List<String> program = new ArrayList<>();
            for(Sentence s: e.getValue()){
                program.add(s.getSentence());
            }
            this.sentences.put(e.getKey().getVoter().getFirstName()+e.getKey().getVoter().getLastName(), program);
        }
        this.errorCode = ErrorCode.SUCCESS;
    }

    public GetSentencesByCandidateListDeoResponse(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public Map<String, List<String>> getSentences() {
        return sentences;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetSentencesByCandidateListDeoResponse that = (GetSentencesByCandidateListDeoResponse) o;
        return Objects.equals(sentences, that.sentences) &&
                errorCode == that.errorCode;
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentences, errorCode);
    }
}
