package net.thumbtack.school.elections.response;

import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class RegisterVoterDtoResponse {
    private String token;
    private ErrorCode error;

    public RegisterVoterDtoResponse(String token) {
        this.token = token;
        this.error = ErrorCode.SUCCESS;
    }

    public RegisterVoterDtoResponse(ErrorCode error) {
        this.error = error;
    }

    public String getToken() {
        return token;
    }

    public ErrorCode getErrorCode() {
        return error;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisterVoterDtoResponse response = (RegisterVoterDtoResponse) o;
        return Objects.equals(token, response.token) &&
                error == response.error;
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, error);
    }
}
