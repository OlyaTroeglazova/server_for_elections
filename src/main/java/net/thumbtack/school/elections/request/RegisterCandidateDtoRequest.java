package net.thumbtack.school.elections.request;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class RegisterCandidateDtoRequest {
    private String firstNameVoter;
    private String lastNameVoter;
    private String firstNameCandidate;
    private String lastNameCandidate;

    public RegisterCandidateDtoRequest(String firstNameVoter, String lastNameVoter, String firstNameCandidate, String lastNameCandidate) {
        this.firstNameVoter = firstNameVoter;
        this.lastNameVoter = lastNameVoter;
        this.firstNameCandidate = firstNameCandidate;
        this.lastNameCandidate = lastNameCandidate;
    }

    public void validate() throws ElectionsException {
        if( firstNameVoter==null || firstNameVoter.equals("") || lastNameCandidate==null || lastNameCandidate.equals("") ||
                firstNameCandidate==null || firstNameCandidate.equals("") || lastNameVoter==null || lastNameVoter.equals("") ){
            throw new ElectionsException(ErrorCode.WRONG_PARAMS);
        }
    }

    public String getFirstNameVoter() {
        return firstNameVoter;
    }

    public String getLastNameVoter() {
        return lastNameVoter;
    }

    public String getFirstNameCandidate() {
        return firstNameCandidate;
    }

    public String getLastNameCandidate() {
        return lastNameCandidate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisterCandidateDtoRequest that = (RegisterCandidateDtoRequest) o;
        return Objects.equals(firstNameVoter, that.firstNameVoter) &&
                Objects.equals(lastNameVoter, that.lastNameVoter) &&
                Objects.equals(firstNameCandidate, that.firstNameCandidate) &&
                Objects.equals(lastNameCandidate, that.lastNameCandidate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstNameVoter, lastNameVoter, firstNameCandidate, lastNameCandidate);
    }
}
