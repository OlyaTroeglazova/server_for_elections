package net.thumbtack.school.elections.request;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class LoginVoterDtoRequest {
    private String login;
    private String password;

    public LoginVoterDtoRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public void validate() throws ElectionsException {
        if( login.equals("") || login == null || password.equals("") || password==null ){
            throw new ElectionsException(ErrorCode.WRONG_PARAMS);
        }
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoginVoterDtoRequest that = (LoginVoterDtoRequest) o;
        return Objects.equals(login, that.login) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, password);
    }
}
