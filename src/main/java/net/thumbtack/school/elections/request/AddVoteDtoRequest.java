package net.thumbtack.school.elections.request;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class AddVoteDtoRequest {
    private String voterFirstName;
    private String voterLastName;
    private String vote;

    public AddVoteDtoRequest(String voterFirstName, String voterLastName, String[] vote) throws ElectionsException {
        this.voterFirstName = voterFirstName;
        this.voterLastName = voterLastName;
        if(vote.length==1){
            this.vote = vote[0];
        }
        else {
            if (vote.length > 2) {
                throw new ElectionsException(ErrorCode.WRONG_PARAMS);
            } else {
                this.vote = vote[0] + vote[1];
            }
        }
    }

    public void validate() throws ElectionsException {
        if(voterFirstName==null || voterFirstName.equals("")
                || voterLastName==null || voterLastName.equals("") || vote.equals("")){
            throw new ElectionsException(ErrorCode.WRONG_PARAMS);
        }
    }

    public String getVoterFirstName() {
        return voterFirstName;
    }

    public String getVoterLastName() {
        return voterLastName;
    }

    public String getVote() {
        return vote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddVoteDtoRequest that = (AddVoteDtoRequest) o;
        return Objects.equals(voterFirstName, that.voterFirstName) &&
                Objects.equals(voterLastName, that.voterLastName) &&
                Objects.equals(vote, that.vote);
    }

    @Override
    public int hashCode() {
        return Objects.hash(voterFirstName, voterLastName, vote);
    }
}
