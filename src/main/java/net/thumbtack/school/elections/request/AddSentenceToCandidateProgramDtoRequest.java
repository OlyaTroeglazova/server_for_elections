package net.thumbtack.school.elections.request;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class AddSentenceToCandidateProgramDtoRequest {
    private String sentence;
    private String firstName;
    private String lastName;

    public AddSentenceToCandidateProgramDtoRequest(String firstName, String lastName, String sentence) {
        this.sentence = sentence;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void validate() throws ElectionsException {
        if(sentence == null || sentence.equals("") || firstName==null || firstName.equals("")
                || lastName==null || lastName.equals("")){
            throw new ElectionsException(ErrorCode.WRONG_PARAMS);
        }
    }

    public String getSentence() {
        return sentence;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddSentenceToCandidateProgramDtoRequest that = (AddSentenceToCandidateProgramDtoRequest) o;
        return Objects.equals(sentence, that.sentence) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentence, firstName, lastName);
    }
}
