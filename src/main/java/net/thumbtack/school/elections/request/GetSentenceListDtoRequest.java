package net.thumbtack.school.elections.request;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class GetSentenceListDtoRequest {
    private String firstName;
    private String lastName;

    public GetSentenceListDtoRequest(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public void validate() throws ElectionsException {
        if( firstName==null || firstName.equals("") || lastName==null || lastName.equals("")){
            throw new ElectionsException(ErrorCode.WRONG_PARAMS);
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetSentenceListDtoRequest that = (GetSentenceListDtoRequest) o;
        return Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName);
    }
}
