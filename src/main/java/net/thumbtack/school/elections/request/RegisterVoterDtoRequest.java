package net.thumbtack.school.elections.request;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class RegisterVoterDtoRequest {
    private String firstName;
    private String lastName;
    private String patronymic;
    private String street;
    private String homeNumber;
    private String apartmentNumber;
    private String login;
    private String password;

    public RegisterVoterDtoRequest(String firstName, String lastName, String patronymic,
                                   String street, String homeNumber, String apartmentNumber, String login, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.street = street;
        this.homeNumber = homeNumber;
        this.apartmentNumber = apartmentNumber;
        this.login = login;
        this.password = password;
    }

    public void validate() throws ElectionsException {
        if( firstName==null || firstName.equals("") || lastName==null || lastName.equals("") ||
                street==null || street.equals("") || login==null || login.equals("") ||
                password==null || password.equals("") || password.length() < 8){
            throw new ElectionsException(ErrorCode.WRONG_PARAMS);
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getStreet() {
        return street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public String getApartmentNumber() {
        return apartmentNumber;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisterVoterDtoRequest that = (RegisterVoterDtoRequest) o;
        return Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Objects.equals(patronymic, that.patronymic) &&
                Objects.equals(street, that.street) &&
                Objects.equals(homeNumber, that.homeNumber) &&
                Objects.equals(apartmentNumber, that.apartmentNumber) &&
                Objects.equals(login, that.login) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, patronymic, street, homeNumber, apartmentNumber, login, password);
    }
}
