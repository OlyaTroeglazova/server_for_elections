package net.thumbtack.school.elections.request;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Objects;

public class EditSentenceMarkDtoRequest {
    private String sentence;
    private String firstName;
    private String lastName;
    private int mark;

    public EditSentenceMarkDtoRequest(String firstName, String lastName, String sentence, int mark) {
        this.sentence = sentence;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mark = mark;
    }

    public void validate() throws ElectionsException {
        if(sentence == null || sentence.equals("") || firstName==null || firstName.equals("")
                || lastName==null || lastName.equals("") || mark > 5 || mark < 1){
            throw new ElectionsException(ErrorCode.WRONG_PARAMS);
        }
    }

    public String getSentence() {
        return sentence;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getMark() {
        return mark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EditSentenceMarkDtoRequest that = (EditSentenceMarkDtoRequest) o;
        return mark == that.mark &&
                Objects.equals(sentence, that.sentence) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sentence, firstName, lastName, mark);
    }
}
