package net.thumbtack.school.elections.request;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.exception.ErrorCode;

import java.util.Arrays;
import java.util.Objects;

public class GetSentencesByCandidateListDeoRequest {
    private String firstName;
    private String lastName;
    private String[] candidatesNames;

    public GetSentencesByCandidateListDeoRequest(String firstName, String lastName, String[] candidates) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.candidatesNames = candidates;
    }

    public void validate() throws ElectionsException {
        if( firstName==null || firstName.equals("") || lastName==null || lastName.equals("") ||
        candidatesNames.length==0 || candidatesNames.length%2==1){
            throw new ElectionsException(ErrorCode.WRONG_PARAMS);
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String[] getCandidates() {
        return candidatesNames;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetSentencesByCandidateListDeoRequest that = (GetSentencesByCandidateListDeoRequest) o;
        return Objects.equals(firstName, that.firstName) &&
                Objects.equals(lastName, that.lastName) &&
                Arrays.equals(candidatesNames, that.candidatesNames);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(firstName, lastName);
        result = 31 * result + Arrays.hashCode(candidatesNames);
        return result;
    }
}
