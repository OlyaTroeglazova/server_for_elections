package net.thumbtack.school.database.mybatis.mappers;

import net.thumbtack.school.database.model.Group;
import net.thumbtack.school.database.model.Subject;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface SubjectMapper {

    @Insert("INSERT INTO subject (name) VALUES (#{subject.name})")
    @Options(useGeneratedKeys = true, keyProperty = "subject.id")
    Integer insert(@Param("subject") Subject subject);

    @Select("SELECT id, name FROM subject WHERE id = #{id}")
    Subject getById(int id);

    @Select("SELECT subject.id, subject.name, subject_group.group_id FROM subject, subject_group where subject.id = subject_group.subject_id and subject_group.group_id = #{group.id}")
    List<Subject> getByGroup(@Param("group")Group group);

    @Select("SELECT id, name FROM subject")
    List<Subject> getAll();

    @Update("UPDATE subject SET name = #{subject.name} WHERE id = #{subject.id} ")
    void update(@Param("subject") Subject subject);

    @Delete("DELETE FROM subject WHERE id = #{subject.id}")
    int delete(@Param("subject") Subject subject);

    @Delete("DELETE FROM subject")
    void deleteAll();

}
