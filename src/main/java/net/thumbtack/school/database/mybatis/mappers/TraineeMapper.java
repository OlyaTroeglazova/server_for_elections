package net.thumbtack.school.database.mybatis.mappers;

import net.thumbtack.school.database.model.Group;
import net.thumbtack.school.database.model.Trainee;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface TraineeMapper {

    @Insert("INSERT INTO trainee (firstname, lastname, rating, group_id) VALUES "
            + "( #{trainee.firstName}, #{trainee.lastName}, #{trainee.rating}, #{group.id} )")
    @Options(useGeneratedKeys = true, keyProperty = "trainee.id")
    Integer insert(@Param ("group") Group group, @Param ("trainee") Trainee trainee);

    @Select("SELECT id, firstname, lastname, rating FROM trainee WHERE id = #{id}")
    Trainee getById(int id);

    @Select("SELECT id, firstname, lastname, rating FROM trainee WHERE group_id = #{group.id}")
    Trainee getByGroup(@Param("group") Group group);

    @Select("SELECT id, firstname, lastname, rating FROM trainee ")
    List<Trainee> getAll();

    @Select({"<script>",
            "SELECT id, firstname, lastname, rating FROM trainee",
            "<where>" +
                    "<if test='firstName != null'> firstname like #{firstName}",
            "</if>",
            "<if test='lastName != null'> and lastname like #{lastName}",
            "</if>",
            "<if test='rating != null'> and rating = #{rating}",
            "</if>",
            "</where>" +
                    "</script>"})
    @Results({@Result(property = "id", column = "id")})
    List<Trainee> getAllWithParams(@Param("firstName") String firstName, @Param("lastName") String lastName, @Param("rating") Integer rating);

    @Update("UPDATE trainee SET firstname = #{trainee.firstName}, lastname = #{trainee.lastName}, rating = #{trainee.rating} WHERE id = #{trainee.id} ")
    void update(@Param("trainee") Trainee trainee);

    @Insert({"<script>",
            "INSERT INTO trainee (firstname, lastname, rating) VALUES ",
            "<foreach item='item' collection='list' separator=','>",
            "( #{item.firstName}, #{item.lastName}, #{item.rating})",
            "</foreach>",
            "</script>"})
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void batchInsert(@Param("list") List<Trainee> traineeList);

    @Delete("DELETE FROM trainee WHERE id = #{trainee.id}")
    int delete(@Param("trainee") Trainee trainee);

    @Delete("DELETE FROM trainee")
    void deleteAll();

}