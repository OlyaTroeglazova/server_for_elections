package net.thumbtack.school.database.jdbc;

import net.thumbtack.school.database.model.Group;
import net.thumbtack.school.database.model.School;
import net.thumbtack.school.database.model.Subject;
import net.thumbtack.school.database.model.Trainee;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcService {

    private static Connection connection;

    public static void insertTrainee(Trainee trainee) throws SQLException {
        connection = JdbcUtils.getConnection();
        String insertQuery = "INSERT INTO trainee(firstname, lastname, rating) VALUES (?,?,?);";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, trainee.getFirstName());
        stmt.setString(2, trainee.getLastName());
        stmt.setInt(3, trainee.getRating());
        stmt.executeUpdate();
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            int id = rs.getInt(1);
            trainee.setId(id);
            rs.close();
        }
    }

    public static void updateTrainee(Trainee trainee) throws SQLException {
        connection = JdbcUtils.getConnection();
        String updateQuery = "UPDATE trainee SET firstname=?, lastname=?, rating=? WHERE id=?;";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(updateQuery);
        stmt.setString(1, trainee.getFirstName());
        stmt.setString(2, trainee.getLastName());
        stmt.setInt(3, trainee.getRating());
        stmt.setInt(4, trainee.getId());
        stmt.executeUpdate();
    }

    public static Trainee getTraineeByIdUsingColNames(int traineeId) throws SQLException {
        connection = JdbcUtils.getConnection();
        String selectQuery = "SELECT * FROM trainee WHERE id=" + traineeId + ";";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(selectQuery);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("id");
            String firstName = rs.getString("firstName");
            String lastName = rs.getString("lastName");
            int raiting = rs.getInt("rating");
            return new Trainee(id, firstName, lastName, raiting);
        }
        return null;
    }

    public static Trainee getTraineeByIdUsingColNumbers(int traineeId) throws SQLException {
        connection = JdbcUtils.getConnection();
        String selectQuery = "SELECT * FROM trainee WHERE id=" + traineeId + ";";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(selectQuery);
        ResultSet rs = stmt.executeQuery(selectQuery);
        while (rs.next()) {
            int id = rs.getInt(1);
            String firstName = rs.getString(2);
            String lastName = rs.getString(3);
            int raiting = rs.getInt(4);
            return new Trainee(id, firstName, lastName, raiting);
        }
        return null;
    }

    public static List<Trainee> getTraineesUsingColNames() throws SQLException {
        connection = JdbcUtils.getConnection();
        String selectQuery = "SELECT * FROM trainee;";
        List<Trainee> trainees = new ArrayList<>();
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(selectQuery);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("id");
            String firstName = rs.getString("firstName");
            String lastName = rs.getString("lastName");
            int raiting = rs.getInt("rating");
            trainees.add(new Trainee(id, firstName, lastName, raiting));
        }
        return trainees;
    }

    public static List<Trainee> getTraineesUsingColNumbers() throws SQLException {
        connection = JdbcUtils.getConnection();
        String selectQuery = "SELECT * FROM trainee;";
        List<Trainee> trainees = new ArrayList<>();
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(selectQuery);
        ResultSet rs = stmt.executeQuery(selectQuery);
        while (rs.next()) {
            int id = rs.getInt(1);
            String firstName = rs.getString(2);
            String lastName = rs.getString(3);
            int raiting = rs.getInt(4);
            trainees.add(new Trainee(id, firstName, lastName, raiting));
        }
        return trainees;
    }

    public static void deleteTrainee(Trainee trainee) throws SQLException {
        connection = JdbcUtils.getConnection();
        String updateQuery = "DELETE FROM trainee WHERE id=?;";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(updateQuery);
        stmt.setInt(1, trainee.getId());
        stmt.executeUpdate();
    }

    public static void deleteTrainees() throws SQLException {
        connection = JdbcUtils.getConnection();
        String updateQuery = "DELETE FROM trainee;";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(updateQuery);
        stmt.executeUpdate();
    }

    public static void insertSubject(Subject subject) throws SQLException {
        connection = JdbcUtils.getConnection();
        String insertQuery = "INSERT INTO subject(name) VALUES (?);";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, subject.getName());
        stmt.executeUpdate();
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            int id = rs.getInt(1);
            subject.setId(id);
            rs.close();
        }
    }

    public static Subject getSubjectByIdUsingColNames(int subjectId) throws SQLException {
        connection = JdbcUtils.getConnection();
        String selectQuery = "SELECT * FROM subject WHERE id=" + subjectId + ";";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(selectQuery);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("id");
            String name = rs.getString("name");
            return new Subject(id, name);
        }
        return null;
    }

    public static Subject getSubjectByIdUsingColNumbers(int subjectId) throws SQLException {
        connection = JdbcUtils.getConnection();
        String selectQuery = "SELECT * FROM subject WHERE id=" + subjectId + ";";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(selectQuery);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            return new Subject(id, name);
        }
        return null;
    }

    public static void deleteSubjects() throws SQLException {
        connection = JdbcUtils.getConnection();
        String updateQuery = "DELETE FROM subject;";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(updateQuery);
        stmt.executeUpdate();
    }

    public static void insertSchool(School school) throws SQLException {
        connection = JdbcUtils.getConnection();
        String insertQuery = "INSERT INTO school(name, year) VALUES (?, ?);";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, school.getName());
        stmt.setInt(2, school.getYear());
        stmt.executeUpdate();
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            int id = rs.getInt(1);
            school.setId(id);
            rs.close();
        }
    }

    public static School getSchoolByIdUsingColNames(int schoolId) throws SQLException {
        connection = JdbcUtils.getConnection();
        String selectQuery = "SELECT * FROM school WHERE id=" + schoolId + ";";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(selectQuery);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("id");
            String name = rs.getString("name");
            int year = rs.getInt("year");
            return new School(id, name, year);
        }
        return null;
    }

    public static School getSchoolByIdUsingColNumbers(int schoolId) throws SQLException {
        connection = JdbcUtils.getConnection();
        String selectQuery = "SELECT * FROM school WHERE id=" + schoolId + ";";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(selectQuery);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            int year = rs.getInt(3);
            return new School(id, name, year);
        }
        return null;
    }

    public static void deleteSchools() throws SQLException {
        connection = JdbcUtils.getConnection();
        String updateQuery = "DELETE FROM school;";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(updateQuery);
        stmt.executeUpdate();
    }

    public static void insertGroup(School school, Group group) throws SQLException {
        connection = JdbcUtils.getConnection();
        String insertQuery = "INSERT INTO `ttschool`.`group` (name, room, school_id) VALUES (?, ?, ?);";
        PreparedStatement stmt = connection.prepareStatement(insertQuery, Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, group.getName());
        stmt.setString(2, group.getRoom());
        stmt.setInt(3, school.getId());
        stmt.executeUpdate();
        ResultSet rs = stmt.getGeneratedKeys();
        if (rs.next()) {
            int id = rs.getInt(1);
            group.setId(id);
            rs.close();
        }
    }

    public static School getSchoolByIdWithGroups(int schoolId) throws SQLException {
        connection = JdbcUtils.getConnection();
        String selectQuery = "SELECT * FROM school JOIN `ttschool`.`group` ON school.id = `ttschool`.`group`.school_id AND school.id = " + schoolId + ";";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(selectQuery);
        ResultSet rs = stmt.executeQuery();
        School school = null;
        while (rs.next()) {
            if (school == null) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                int year = rs.getInt(3);
                school = new School(id, name, year);
            }
            int groupId = rs.getInt(4);
            String groupName = rs.getString(5);
            String room = rs.getString(6);
            Group group = new Group(groupId, groupName, room);
            school.addGroup(group);
        }
        return school;
    }

    public static List<School> getSchoolsWithGroups() throws SQLException {
        connection = JdbcUtils.getConnection();
        List<School> schools = new ArrayList<>();
        String selectQuery = "SELECT * FROM school JOIN `ttschool`.`group` ON school.id = `ttschool`.`group`.school_id;";
        PreparedStatement stmt = (PreparedStatement) connection.prepareStatement(selectQuery);
        ResultSet rs = stmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt(1);
            String name = rs.getString(2);
            int year = rs.getInt(3);
            School school = new School(id, name, year);
            int groupId = rs.getInt(4);
            String groupName = rs.getString(5);
            String room = rs.getString(6);
            Group group = new Group(groupId, groupName, room);
            boolean contains = false;
            for (School s : schools) {
                if (s.getId() == school.getId()) {
                    s.addGroup(group);
                    contains = true;
                }
            }
            if (!contains) {
                school.addGroup(group);
                schools.add(school);
            }
        }
        return schools;
    }
}