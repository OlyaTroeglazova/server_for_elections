package net.thumbtack.school.elections;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.exception.ErrorCode;
import net.thumbtack.school.elections.response.*;
import net.thumbtack.school.elections.server.Server;
import org.junit.*;

import java.io.IOException;

public class ElectionsMethodsTest extends BaseElectionsTest {

    @BeforeClass
    public static void startServer() throws IOException {
        Server.startServer("123.txt");
    }

    @AfterClass
    public static void stopServer() throws IOException {
        Server.stopServer("123.txt");
    }

    @Test
    public void insertVoterTest1() throws ElectionsException {
        RegisterVoterDtoResponse voter = insertVoter("Ivan", "Ivanov", "Ivanovich",
                "Mira", "55a", "12", "ivanov55", "ivandivan");
        Assert.assertEquals(voter.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void insertVoterWrongTest1() throws ElectionsException {
        RegisterVoterDtoResponse voter = insertVoter("", "Ivanov", "Ivanovich",
                "Mira", "55a", "12", "ivanov55", "ivandivan");
        Assert.assertEquals(voter.getErrorCode(), ErrorCode.WRONG_PARAMS);
    }

    @Test
    public void insertVoterWrongTest2() throws ElectionsException {
        RegisterVoterDtoResponse voter = insertVoter("Ivan", "", "Ivanov",
                "Mira", "55a", "12", "ivanov55", "ivandivan");
        Assert.assertEquals(voter.getErrorCode(), ErrorCode.WRONG_PARAMS);
    }

    @Test
    public void insertVoterWrongTest3() throws ElectionsException {
        RegisterVoterDtoResponse voter = insertVoter("Ivan", "Ivanov", "Ivanovich",
                "", "55a", "12", "ivanov55", "ivandivan");
        Assert.assertEquals(voter.getErrorCode(), ErrorCode.WRONG_PARAMS);
    }

    @Test
    public void insertVoterWrongTest4() throws ElectionsException {
        RegisterVoterDtoResponse voter = insertVoter("Ivan", "Ivanov", "Ivanovich",
                "Mira", "55a", "12", "ivanov55", "ivandivan");
        Assert.assertEquals(voter.getErrorCode(), ErrorCode.VOTER_ALREADY_EXISTS);
    }

    @Test
    public void insertVoterWrongTest5() throws ElectionsException {
        RegisterVoterDtoResponse voter = insertVoter("Ivan", "Ivanov", "Ivanovich",
                "Mira", "55a", "12", "ivanivan", "ivandivan");
        Assert.assertEquals(voter.getErrorCode(), ErrorCode.VOTER_ALREADY_EXISTS);
    }

    @Test
    public void insertVoterWrongTest6() throws ElectionsException {
        RegisterVoterDtoResponse voter = insertVoter("Vanya", "Ivanovich", "Ivanov",
                "Mira", "55a", "12", "ivanov55", "ivandivan");
        Assert.assertEquals(voter.getErrorCode(), ErrorCode.VOTER_ALREADY_EXISTS);
    }

    @Test
    public void insertVoterWrongTest7() throws ElectionsException {
        RegisterVoterDtoResponse voter = insertVoter("Stepan", "Stepanov", "Ivanov",
                "Marksa", "18", null,  "ivanov55", "ivandivan");
        Assert.assertEquals(voter.getErrorCode(), ErrorCode.VOTER_ALREADY_EXISTS);
    }

    @Test
    public void getVoterTest() throws ElectionsException {
        GetVoterDtoResponse response = getVoterByName("Ivan", "Ivanov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void getVoterWrongTest1() throws ElectionsException {
        GetVoterDtoResponse response = getVoterByName("Oleg", "Ivanovich");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.VOTER_NOT_FOUND);
    }

    @Test
    public void getVoterWrongTest2() throws ElectionsException {
        GetVoterDtoResponse response = getVoterByName("", "Ivanov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.WRONG_PARAMS);
    }

    @Test
    public void getVoterWrongTest3() throws ElectionsException {
        GetVoterDtoResponse response = getVoterByName("Ivan", "");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.WRONG_PARAMS);
    }

    @Test
    public void logoutVoterTest1() throws ElectionsException {
        insertVoter("Pavel", "Petrov", "Andreevich",
                "Mira", "45", "", "petrovP", "p1e2t3r4o5v6");
        GetVoterDtoResponse response = getVoterByName("Pavel", "Petrov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
        LogoutVoterDtoResponse logoutVoterDtoResponse = logoutVoter("Pavel", "Petrov");
        Assert.assertEquals(logoutVoterDtoResponse.getErrorCode(), ErrorCode.SUCCESS);
        response = getVoterByName("Pavel", "Petrov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.VOTER_NOT_FOUND);
    }

    @Test
    public void logoutVoterWrongTest1() throws ElectionsException {
        insertVoter("Petr", "Petrov", "Petrovich",
                "Mira", "45", "", "petrovPetr", "p1e2t3r4o5v6");
        logoutVoter("Petr", "Petrov");
        LogoutVoterDtoResponse response = logoutVoter("Petr", "Petrov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.VOTER_NOT_FOUND);
    }

    @Test
    public void loginVoterTest1() throws ElectionsException {
        insertVoter("Anna", "Korobkina", "Sergeevna",
                "1905 goda", "9", "109", "annaKorob", "Anna1905109");
        logoutVoter("Anna", "Korobkina");
        LoginVoterDtoResponse response = loginVoter("annaKorob", "Anna1905109");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void loginVoterWrongTest1() throws ElectionsException {
        LoginVoterDtoResponse response = loginVoter("ivanov55", "ivandivan");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.VOTER_ALREADY_EXISTS);
    }

    @Test
    public void loginVoterWrongTest2() throws ElectionsException {
        LoginVoterDtoResponse response = loginVoter("ivanov565", "ivandivan");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.VOTER_NOT_FOUND);
    }

    @Test
    public void insertSentenceTest() throws ElectionsException {
        insertVoter("Pavel", "Ivanov", "Andreevich",
                "Mira", "45", "", "pavelivanov", "pavelivv");
        InsertSentenceDtoResponse response1 = insertSentence("Pavel", "Ivanov", "More flowers");
        Assert.assertEquals(response1.getErrorCode(), ErrorCode.SUCCESS);
        InsertSentenceDtoResponse response2 = insertSentence("Pavel", "Ivanov", "More skyscrapers");
        Assert.assertEquals(response2.getErrorCode(), ErrorCode.SUCCESS);
        InsertSentenceDtoResponse response3 = insertSentence("Pavel", "Ivanov", "Salary increase");
        Assert.assertEquals(response3.getErrorCode(), ErrorCode.SUCCESS);
        InsertSentenceDtoResponse response4 = insertSentence("Pavel", "Ivanov", "Prices lower");
        Assert.assertEquals(response4.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void insertSentenceWrongTestTest() throws ElectionsException {
        insertVoter("Fedor", "Fedorov", "Petrovich",
                "Marksa", "12", "4", "fedorFedor", "fff987fedor432");
        InsertSentenceDtoResponse response = insertSentence("Fedor", "Fedorov", "More flowers");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SENTENCE_ALREADY_EXISTS);
    }

    @Test
    public void addSentenceMarkTest() throws ElectionsException {
        insertVoter("Anna", "Ananasova", "Alekseevna",
                "Marksa", "12", "4", "ananasovaa", "vfhncd5432234");
        AddSentenceMarkDtoResponse response = addMark("Anna", "Ananasova", "More flowers", 4);
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void addSentenceMarkWrongTest1() throws ElectionsException {
        insertVoter("Vitaly", "Fedorov", "Petrovich",
                "Marksa", "12", "4", "vitalyaFfff", "vvvviiitaallya");
        AddSentenceMarkDtoResponse response = addMark("Vitaly", "Fedorov", "More flowerrs", 4);
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SENTENCE_NOT_FOUND);
    }

    @Test
    public void addSentenceMarkWrongTest2() throws ElectionsException {
        insertVoter("Vladimir", "Alexeev", "Petrovich",
                "Marksa", "12", "4", "vlvlvlvlvl", "vladimiralexeev0987");
        AddSentenceMarkDtoResponse response = addMark("Vladimir", "Alexeev", "More flowers", 0);
        Assert.assertEquals(response.getErrorCode(), ErrorCode.WRONG_PARAMS);
    }

    @Test
    public void editSentenceMarkTest() throws ElectionsException {
        insertVoter("Vladimir", "Alexeev", "Petrovich",
                "Marksa", "12", "4", "vlvlvlvlvl", "vladimiralexeev0987");
        addMark("Vladimir", "Alexeev", "More flowers", 4);
        EditSentenceMarkDtoResponse response = editMark("Vladimir", "Alexeev", "More flowers", 2);
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void removeSentenceMarkTest() throws ElectionsException {
        RemoveSentenceMarkDtoResponse response = removeMark("Vladimir", "Alexeev", "More flowers");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void removeSentenceMarkWrongTest() throws ElectionsException {
        RemoveSentenceMarkDtoResponse response = removeMark("Pavel", "Ivanov", "More flowers");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.CANNOT_REMOVE_MARK);
    }

    @Test
    public void editSentenceMarkWrongTest() throws ElectionsException {
        EditSentenceMarkDtoResponse response = editMark("Pavel", "Ivanov", "More flowers", 2);
        Assert.assertEquals(response.getErrorCode(), ErrorCode.CANNOT_CHANGE_MARK);
    }

    @Test
    public void insertCandidateTest1() throws ElectionsException {
        RegisterCandidateDtoResponse response = registerCandidate("Vladimir", "Alexeev", "Vladimir", "Alexeev");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void insertCandidateTest2() throws ElectionsException {
        RegisterCandidateDtoResponse response = registerCandidate("Vladimir", "Alexeev", "Pavel", "Ivanov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void addSentenceToCandidateProgramTest() throws ElectionsException {
        AddSentenceToCandidateProgramDtoResponse response = addSentenceToCandidateProgram("Vladimir", "Alexeev", "More flowers");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void removeSentenceTest2() throws ElectionsException {
        RemoveSentenceDtoResponse response = removeSentence("Vladimir", "Alexeev", "More flowers");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void insertCandidateWrongTestTest() throws ElectionsException {
        RegisterCandidateDtoResponse response = registerCandidate("Vladimir", "Alexeev", "Pavel", "Ivanov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.CANDIDATE_ALREADY_EXISTS);
    }

    @Test
    public void setAgreementToNominateTest() throws ElectionsException {
        SetAgreementToNominateDtoResponse response = setAgreement("Pavel", "Ivanov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void setAgreementToNominateWrongTest() throws ElectionsException {
        SetAgreementToNominateDtoResponse response = setAgreement("Ivan", "Ivanov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.CANDIDATE_NOT_FOUND);
    }

    @Test
    public void removeAgreementToNominateTest() throws ElectionsException {
        RemoveAgreementToNominateDtoResponse response = removeAgreement("Pavel", "Ivanov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void logoutCandidateTest() throws ElectionsException {
        RegisterVoterDtoResponse voter = insertVoter("Anastasia", "Ivanova", "",
            "Mira", "55a", "12", "ivanovaA", "nas123456tya");
        Assert.assertEquals(voter.getErrorCode(), ErrorCode.SUCCESS);
        RegisterCandidateDtoResponse registerResponse = registerCandidate("Anastasia", "Ivanova", "Anastasia", "Ivanova");
        Assert.assertEquals(registerResponse.getErrorCode(), ErrorCode.SUCCESS);
        LogoutVoterDtoResponse logoutResponse1 = logoutVoter("Anastasia", "Ivanova");
        Assert.assertEquals(logoutResponse1.getErrorCode(), ErrorCode.CANNOT_LOGOUT_CANDIDATE);
        RemoveAgreementToNominateDtoResponse removeAgreementResponse = removeAgreement("Anastasia", "Ivanova");
        Assert.assertEquals(removeAgreementResponse.getErrorCode(), ErrorCode.SUCCESS);
        LogoutVoterDtoResponse logoutResponse2 = logoutVoter("Anastasia", "Ivanova");
        Assert.assertEquals(logoutResponse2.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void getCandidatesListTest() throws ElectionsException {
        GetCandidatesListDtoResponse response = getCandidatesList("Pavel", "Ivanov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void getSentenceListTest() throws ElectionsException {
        GetSentenceListDtoResponse response = getSentenceList("Pavel", "Ivanov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void getSentencesByCandidateListTest() throws ElectionsException {
        GetSentencesByCandidateListDeoResponse response = getSentencesByCandidateList("Pavel", "Ivanov", "Pavel", "Ivanov");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.SUCCESS);
    }

    @Test
    public void getSentencesByCandidateListWrongTest() throws ElectionsException {
        GetSentencesByCandidateListDeoResponse response = getSentencesByCandidateList("Pavel", "Ivanov", "Pavel");
        Assert.assertEquals(response.getErrorCode(), ErrorCode.WRONG_PARAMS);
    }

}