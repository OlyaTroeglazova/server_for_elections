package net.thumbtack.school.elections;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.exception.ErrorCode;
import net.thumbtack.school.elections.model.Candidate;
import net.thumbtack.school.elections.request.*;
import net.thumbtack.school.elections.response.*;
import net.thumbtack.school.elections.server.Server;
import org.junit.Assert;

class BaseElectionsTest {
    private static final Gson gson = new GsonBuilder().create();

    static RegisterVoterDtoResponse insertVoter(String firstName, String lastName, String patronymic, String street,
                                       String homeNumber, String apartmentNumber, String login, String password) throws ElectionsException {
        RegisterVoterDtoRequest request = new RegisterVoterDtoRequest(firstName, lastName, patronymic, street, homeNumber, apartmentNumber, login, password);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.registerVoter(jsonRequest);
        RegisterVoterDtoResponse response = gson.fromJson(jsonResponse, RegisterVoterDtoResponse.class);
        return response;
    }

    static GetVoterDtoResponse getVoterByName(String firstName, String lastName) throws ElectionsException {
        GetVoterDtoRequest request = new GetVoterDtoRequest(firstName, lastName);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.getVoter(jsonRequest);
        GetVoterDtoResponse response = gson.fromJson(jsonResponse, GetVoterDtoResponse.class);
        if(response.getErrorCode().equals(ErrorCode.SUCCESS)) {
            Assert.assertEquals(response.getFirstName(), firstName);
            Assert.assertEquals(response.getLastName(), lastName);
        }
        return response;
    }

    static LoginVoterDtoResponse loginVoter(String login, String password) throws ElectionsException {
        LoginVoterDtoRequest request = new LoginVoterDtoRequest(login, password);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.loginVoter(jsonRequest);
        LoginVoterDtoResponse response = gson.fromJson(jsonResponse, LoginVoterDtoResponse.class);
        return response;
    }

    static LogoutVoterDtoResponse logoutVoter(String firstName, String lastName) throws ElectionsException {
        LogoutVoterDtoRequest request = new LogoutVoterDtoRequest(firstName, lastName);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.logoutVoter(jsonRequest);
        LogoutVoterDtoResponse response = gson.fromJson(jsonResponse, LogoutVoterDtoResponse.class);
        return response;
    }

    static InsertSentenceDtoResponse insertSentence(String firstName, String lastName, String sentence) throws ElectionsException {
        InsertSentenceDtoRequest request = new InsertSentenceDtoRequest(firstName, lastName, sentence);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.insertSentence(jsonRequest);
        InsertSentenceDtoResponse response = gson.fromJson(jsonResponse, InsertSentenceDtoResponse.class);
        if (response.getErrorCode().equals(ErrorCode.SUCCESS)) {
            Assert.assertEquals(response.getAuthor().getFirstName(), firstName);
            Assert.assertEquals(response.getAuthor().getLastName(), lastName);
            Assert.assertEquals(response.getSentence(), sentence);
        }
        return response;
    }

    static AddSentenceToCandidateProgramDtoResponse addSentenceToCandidateProgram(String firstName, String lastName, String sentence) throws ElectionsException {
        AddSentenceToCandidateProgramDtoRequest request = new AddSentenceToCandidateProgramDtoRequest(firstName, lastName, sentence);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.addSentenceToCandidateProgram(jsonRequest);
        AddSentenceToCandidateProgramDtoResponse response = gson.fromJson(jsonResponse, AddSentenceToCandidateProgramDtoResponse.class);
        return response;
    }

    static RemoveSentenceDtoResponse removeSentence(String firstName, String lastName, String sentence) throws ElectionsException {
        RemoveSentenceDtoRequest request = new RemoveSentenceDtoRequest(firstName, lastName, sentence);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.removeSentence(jsonRequest);
        RemoveSentenceDtoResponse response = gson.fromJson(jsonResponse, RemoveSentenceDtoResponse.class);
        return response;
    }

    static AddSentenceMarkDtoResponse addMark(String firstName, String lastName, String sentence, int mark) throws ElectionsException {
        AddSentenceMarkDtoRequest request = new AddSentenceMarkDtoRequest(firstName, lastName, sentence, mark);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.addSentenceMark(jsonRequest);
        AddSentenceMarkDtoResponse response = gson.fromJson(jsonResponse, AddSentenceMarkDtoResponse.class);
        return response;
    }

    static EditSentenceMarkDtoResponse editMark(String firstName, String lastName, String sentence, int mark) throws ElectionsException {
        EditSentenceMarkDtoRequest request = new EditSentenceMarkDtoRequest(firstName, lastName, sentence, mark);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.editSentenceMark(jsonRequest);
        EditSentenceMarkDtoResponse response = gson.fromJson(jsonResponse, EditSentenceMarkDtoResponse.class);
        return response;
    }

    static RemoveSentenceMarkDtoResponse removeMark(String firstName, String lastName, String sentence) throws ElectionsException {
        RemoveSentenceMarkDtoRequest request = new RemoveSentenceMarkDtoRequest(firstName, lastName, sentence);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.removeSentenceMark(jsonRequest);
        RemoveSentenceMarkDtoResponse response = gson.fromJson(jsonResponse, RemoveSentenceMarkDtoResponse.class);
        return response;
    }

    static RegisterCandidateDtoResponse registerCandidate(String firstNameVoter, String lastNameVoter,
                                                                     String firstNameCandidate, String lastNameCandidate) throws ElectionsException {
        RegisterCandidateDtoRequest request = new RegisterCandidateDtoRequest(firstNameVoter, lastNameVoter, firstNameCandidate, lastNameCandidate);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.registerCandidate(jsonRequest);
        RegisterCandidateDtoResponse response = gson.fromJson(jsonResponse, RegisterCandidateDtoResponse.class);
        return response;
    }

    static SetAgreementToNominateDtoResponse setAgreement(String firstName, String lastName) throws ElectionsException {
        SetAgreementToNominateDtoRequest request = new SetAgreementToNominateDtoRequest(firstName, lastName);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.setAgreementToNominate(jsonRequest);
        SetAgreementToNominateDtoResponse response = gson.fromJson(jsonResponse, SetAgreementToNominateDtoResponse.class);
        return response;
    }

    static RemoveAgreementToNominateDtoResponse removeAgreement(String firstName, String lastName) throws ElectionsException {
        RemoveAgreementToNominateDtoRequest request = new RemoveAgreementToNominateDtoRequest(firstName, lastName);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.removeAgreementToNominate(jsonRequest);
        RemoveAgreementToNominateDtoResponse response = gson.fromJson(jsonResponse, RemoveAgreementToNominateDtoResponse.class);
        return response;
    }

    static GetCandidatesListDtoResponse getCandidatesList(String firstName, String lastName) throws ElectionsException {
        GetCandidatesListDtoRequest request = new GetCandidatesListDtoRequest(firstName, lastName);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.getCandidatesList(jsonRequest);
        GetCandidatesListDtoResponse response = gson.fromJson(jsonResponse, GetCandidatesListDtoResponse.class);
        return response;
    }

    static GetSentenceListDtoResponse getSentenceList(String firstName, String lastName) throws ElectionsException {
        GetSentenceListDtoRequest request = new GetSentenceListDtoRequest(firstName, lastName);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.getSentenceList(jsonRequest);
        GetSentenceListDtoResponse response = gson.fromJson(jsonResponse, GetSentenceListDtoResponse.class);
        return response;
    }

    static GetSentencesByCandidateListDeoResponse getSentencesByCandidateList(String firstName, String lastName, String ... candidateName) throws ElectionsException {
        GetSentencesByCandidateListDeoRequest request = new GetSentencesByCandidateListDeoRequest(firstName, lastName, candidateName);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.getSentencesByCandidateList(jsonRequest);
        GetSentencesByCandidateListDeoResponse response = gson.fromJson(jsonResponse, GetSentencesByCandidateListDeoResponse.class);
        return response;
    }

    static StartElectionsDtoResponse startElections() throws ElectionsException {
        StartElectionsDtoRequest request = new StartElectionsDtoRequest();
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.startElections(jsonRequest);
        StartElectionsDtoResponse response = gson.fromJson(jsonResponse, StartElectionsDtoResponse.class);
        return response;
    }

    static StopElectionsDtoResponse stopElections() throws ElectionsException {
        StopElectionsDtoRequest request = new StopElectionsDtoRequest();
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.stopElections(jsonRequest);
        StopElectionsDtoResponse response = gson.fromJson(jsonResponse, StopElectionsDtoResponse.class);
        return response;
    }

    static AddVoteDtoResponse addVote(String voterFirstName, String voterLastName, String ... vote) throws ElectionsException {
        AddVoteDtoRequest request = new AddVoteDtoRequest(voterFirstName, voterLastName, vote);
        String jsonRequest = gson.toJson(request);
        String jsonResponse = Server.addVote(jsonRequest);
        AddVoteDtoResponse response = gson.fromJson(jsonResponse, AddVoteDtoResponse.class);
        return response;
    }

}