package net.thumbtack.school.elections;

import net.thumbtack.school.elections.exception.ElectionsException;
import net.thumbtack.school.elections.exception.ErrorCode;
import net.thumbtack.school.elections.response.*;
import net.thumbtack.school.elections.server.Server;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.Serializable;

public class ElectionsTest extends BaseElectionsTest{

    @Test
    public void test() throws ElectionsException, IOException {
        Server.startServer("2.txt");
        RegisterVoterDtoResponse insertVoter1 = insertVoter("Voter1", "Voter1", "Voter1",
                "Street1", "1", "1", "voter1voter1", "votervoter1");
        RegisterVoterDtoResponse insertVoter2 = insertVoter("Voter2", "Voter2", "Voter2",
                "Street2", "2", "2", "voter2voter2", "votervoter2");
        RegisterVoterDtoResponse insertVoter3 = insertVoter("Voter3", "Voter3", "Voter3",
                "Street3", "3", "3", "voter3voter3", "votervoter3");
        RegisterVoterDtoResponse insertVoter4 =  insertVoter("Voter4", "Voter4", "Voter4",
                "Street4", "4", "4", "voter4voter4", "votervoter4");
        RegisterVoterDtoResponse insertVoter5 = insertVoter("Voter5", "Voter5", "Voter5",
                "Street5", "5", "5", "voter5voter5", "votervoter5");
        RegisterVoterDtoResponse insertVoter6 = insertVoter("Voter6", "Voter6", "Voter6",
                "Street6", "6", "6", "voter6voter6", "votervoter6");
        RegisterVoterDtoResponse insertVoter7 = insertVoter("Voter7", "Voter7", "Voter7",
                "Street7", "7", "7", "voter7voter7", "votervoter7");
        RegisterVoterDtoResponse insertVoter8 = insertVoter("Voter8", "Voter8", "Voter8",
                "Street8", "8", "8", "voter8voter8", "votervoter8");
        RegisterVoterDtoResponse insertVoter9 = insertVoter("Candidate1", "Candidate1", "Candidate1",
                "StreetCandidate1", "1", "1", "candidate1", "1candidate");
        RegisterVoterDtoResponse insertVoter10 = insertVoter("Candidate2", "Candidate2", "Candidate2",
                "StreetCandidate2", "2", "2", "candidate2", "2candidate");
        RegisterVoterDtoResponse insertVoter11 = insertVoter("Candidate3", "Candidate3", "Candidate3",
                "StreetCandidate3", "3", "3", "candidate3", "3candidate");

        LogoutVoterDtoResponse logoutVoter7 = logoutVoter("Voter7", "Voter7");
        LogoutVoterDtoResponse logoutVoter8 = logoutVoter("Voter8", "Voter8");
        LoginVoterDtoResponse loginVoter8 = loginVoter("voter8voter8", "votervoter8");

        RegisterCandidateDtoResponse registerCandidate1 = registerCandidate("Voter1", "Voter1",
                "Candidate1", "Candidate1");
        SetAgreementToNominateDtoResponse setAgreement1 = setAgreement("Candidate1", "Candidate1");
        RegisterCandidateDtoResponse registerCandidate2 = registerCandidate("Voter2", "Voter2",
                "Candidate2", "Candidate2");
        SetAgreementToNominateDtoResponse setAgreement2 = setAgreement("Candidate2", "Candidate2");
        RemoveAgreementToNominateDtoResponse removeAgreement = removeAgreement("Candidate2", "Candidate2");
        RegisterCandidateDtoResponse registerCandidate3 = registerCandidate("Candidate3", "Candidate3",
                "Candidate3", "Candidate3");

        InsertSentenceDtoResponse insertSentence1 = insertSentence("Voter5", "Voter5", "Sentence1ByVoter5");
        InsertSentenceDtoResponse insertSentence2 = insertSentence("Voter6", "Voter6", "Sentence1ByVoter6");
        InsertSentenceDtoResponse insertSentence3 = insertSentence("Voter8", "Voter8", "Sentence1ByVoter8");
        InsertSentenceDtoResponse insertSentence4 = insertSentence("Candidate1", "Candidate1", "Sentence1ByCandidate1");
        InsertSentenceDtoResponse insertSentence5 = insertSentence("Candidate3", "Candidate3", "Sentence1ByCandidate3");

        AddSentenceMarkDtoResponse addMark1 = addMark("Voter1", "Voter1", "Sentence1ByVoter5", 3);
        AddSentenceMarkDtoResponse addMark2 = addMark("Voter2", "Voter2", "Sentence1ByVoter5", 4);
        AddSentenceMarkDtoResponse addMark3 = addMark("Voter3", "Voter3", "Sentence1ByVoter5", 5);
        AddSentenceMarkDtoResponse addMark4 = addMark("Candidate1", "Candidate1", "Sentence1ByVoter5", 5);
        AddSentenceToCandidateProgramDtoResponse addSentenceToProgram1 = addSentenceToCandidateProgram("Candidate1", "Candidate1", "Sentence1ByVoter5");
        AddSentenceMarkDtoResponse addMark5 = addMark("Candidate1", "Candidate1", "Sentence1ByVoter6", 5);
        AddSentenceToCandidateProgramDtoResponse addSentenceToProgram2 = addSentenceToCandidateProgram("Candidate1", "Candidate1", "Sentence1ByVoter6");
        RemoveSentenceDtoResponse removeSentence = removeSentence("Candidate1", "Candidate1", "Sentence1ByVoter6");
        AddSentenceMarkDtoResponse addMark6 = addMark("Candidate3", "Candidate3", "Sentence1ByVoter8", 5);
        AddSentenceToCandidateProgramDtoResponse addSentenceToProgram3 = addSentenceToCandidateProgram("Candidate3", "Candidate3", "Sentence1ByVoter8");

        StartElectionsDtoResponse startElections = startElections();

        AddVoteDtoResponse vote1 = addVote("Voter1", "Voter1", "Against all");
        AddVoteDtoResponse vote2 = addVote("Voter2", "Voter2", "Candidate1", "Candidate1");
        AddVoteDtoResponse vote3 = addVote("Voter3", "Voter3", "Candidate3", "Candidate3");
        AddVoteDtoResponse vote4 = addVote("Voter4", "Voter4", "Candidate3", "Candidate3");
        AddVoteDtoResponse vote5 = addVote("Voter5", "Voter5", "Candidate3", "Candidate3");
        AddVoteDtoResponse vote6 = addVote("Voter6", "Voter6", "Candidate1", "Candidate1");
        AddVoteDtoResponse vote8 = addVote("Voter8", "Voter8", "Against all");
        AddVoteDtoResponse vote9 = addVote("Candidate1", "Candidate1", "Candidate3", "Candidate3");

        StopElectionsDtoResponse stopElections = stopElections();

        Assert.assertEquals(insertVoter1.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertVoter2.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertVoter3.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertVoter4.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertVoter5.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertVoter6.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertVoter7.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertVoter8.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertVoter9.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertVoter10.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertVoter11.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(logoutVoter7.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(logoutVoter8.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(loginVoter8.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(registerCandidate1.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(setAgreement1.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(registerCandidate2.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(setAgreement2.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(removeAgreement.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(registerCandidate3.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertSentence1.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertSentence2.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertSentence3.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertSentence4.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(insertSentence5.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(addMark1.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(addMark2.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(addMark3.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(addMark4.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(addMark5.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(addMark6.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(addSentenceToProgram1.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(addSentenceToProgram2.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(addSentenceToProgram3.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(removeSentence.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(startElections.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(vote1.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(vote2.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(vote3.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(vote4.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(vote5.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(vote6.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(vote8.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(vote9.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(stopElections.getErrorCode(), ErrorCode.SUCCESS);
        Assert.assertEquals(stopElections.getFirstName(), "Candidate3");
        Assert.assertEquals(stopElections.getLastName(), "Candidate3");
        Server.stopServer("2.txt");
    }

}